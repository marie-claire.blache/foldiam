/* Marie-Claire Blache, PIC - PRC INRAe , marie-claire.blache@inrae.fr 
 * mars 2023
 * 
 * Requirement:
 * 	- a folder with "tif" images . 
 * 	
 * How to use:
 * 	- start the macro
 * 	- select folder 
 * 	- once the macro is finished, a folder named "Result" is created. It contains : 
 * 		- txtresults.txt with results of all images
 * 		-mask for each images
 * 		- flatten for each images 
 * 		- roi to use
 * 
*/

//////function/////

//function to create txt file texte for results.

function txt(s1,s2,title1){
	title2 = "["+title1+"]";
	f = title2;
	if (isOpen(title1)){
		print(f, s2 + s2);
		}
	else {
	run("Text Window...", "name="+title2+" width=100 height=8 menu");
	print(f, s1 + s2 + s2); 
//	return 1
		}
	}   


/////////////////macro start/////////////////////////////////////

//Choose your folder with images to analyze

dir = getDirectory("folder with images to analyze");
list = getFileList(dir);
list = Array.sort(list);

// Create folder with result
sauve = dir + "Result" + File.separator;
File.makeDirectory(sauve);

//
run("Set Measurements...", "area fit feret's display redirect=None decimal=3");

for (i=0; i<list.length; i++){
	pathi = dir + list[i];
	print(pathi);
	name = File.getNameWithoutExtension(pathi);
	print(name); 
	pathmask = sauve + name + "_mask.tif" ;
	showProgress(i, list.length);
	if (endsWith(pathi,".tif") && File.exists(pathmask) != 1){
		open(pathi); 
		
		//calibration
		//run("Set Scale...", "distance=1 known=0.645 unit=µm");
		
		//process image
		run("Invert");
		run("Subtract Background...", "rolling=1000");
		run("Median...", "radius=2");
		run("Split Channels");
		selectImage(1); 
		red = getTitle();
		selectImage(2); 
		green = getTitle();
		selectImage(3);
		blue = getTitle();
		selectWindow(red);
		close(); 
		selectWindow(blue);
		close(); 
		selectWindow(green);
		run("Gaussian Blur...", "sigma=8");
		
		//create mask with automatique threshold
		setAutoThreshold("Minimum dark");
		setOption("BlackBackground", false);
		run("Convert to Mask");
		saveAs("Tiff", sauve  + name + "_mask"); 	
		run("Analyze Particles...", "size=5000-Infinity pixel circularity=0-1.00 display add");
		roiManager("Show All");
		nroi = roiManager("count"); 
		print(nroi); 
		
		if(roiManager("count") == 0){
			//roiManager("save", sauve + name + ".roi"); 
			result = newArray(name, NaN , NaN , NaN, NaN,"\n");
			print(result[0]); 
			tabtitle = newArray( "Images" , "Area" , "Major" , "Minor", "circularité", "\n");
			s1 = String.join(tabtitle,"\t"); 
			s2 = String.join(result, "\t"); 
			title1 = "results";
			txt(s1,s2,title1);	
			}
		else{
			max_size = 0;
			n = roiManager("count"); 
			//Area max
			for (r = 0; r < n; r++) { 
				roiManager("Select", r); 
				getStatistics(area);
				if (area >  max_size){ max_size = area;} 
			}
			print(max_size); 
			
			//delete roi if to much rois
			to_be_deleted = newArray(); 
			for (r = 0; r < n; r++) { 
				roiManager("Select", r); 
				getStatistics(area);
				if (area < max_size){
					to_be_deleted = Array.concat(to_be_deleted, r);
				}
			}   
			if (to_be_deleted.length > 0){
			roiManager("Select", to_be_deleted);
			roiManager("Delete");
			}
			
			//extract value for roi
			roiManager("Select", 0); 	
			run("Measure");
			area = getValue("Area");
			major = getValue("Major");
			minor = getValue("Minor");
			circ = getValue("Circ.");
			roiManager("save", sauve + name + ".roi");
			result = newArray( name, area , major , minor, circ,"\n");
			print(result[4]); 
			tabtitle = newArray( "Images" , "Area" , "Major" , "Minor", "Circ", "\n");
			s1 = String.join(tabtitle,"\t"); 
			s2 = String.join(result, "\t"); 
			title1 = "results";
			txt(s1,s2,title1);	
			
			//images with Roi flatten
			open(pathi); 
			roiManager("Select", 0); 
			run("Flatten");
			saveAs("Tiff", sauve  + name); 
			
			//delete all 
			roiManager("Deselect");
			roiManager("Delete");
			run("Close All");			
			
		}
	}}
	
//save txt
selectWindow("results");
saveAs("txt", sauve  + "txtresults"); 
run("Close");
showMessage("Done!");